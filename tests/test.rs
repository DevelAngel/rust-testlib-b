extern crate testlib_b;

#[test]
fn test_b_func() {
    assert_eq!(testlib_b::test_b(0), 0);
    assert_eq!(testlib_b::test_b(1), 3);
    assert_eq!(testlib_b::test_b(2), 6);
    assert_eq!(testlib_b::test_b(3), 9);
}
