pub fn test_b(b: i32) -> i32 {
    b * 3
}


#[cfg(test)]
mod tests {
    #[test]
    fn test_b() {
        assert_eq!(super::test_b(0), 0);
        assert_eq!(super::test_b(1), 3);
        assert_eq!(super::test_b(2), 6);
        assert_eq!(super::test_b(3), 9);
    }
}
